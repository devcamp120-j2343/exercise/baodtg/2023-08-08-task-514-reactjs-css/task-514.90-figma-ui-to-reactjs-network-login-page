import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import logo from './assets/images/logo.png'
import picture from './assets/images/login-illustrator.png'

import './App.css';

function App() {
  return (
    <div className="container-fluid">
      <div className='row'>
        <div className='col-lg-4 div-form text-center'>

          <img src={logo}></img>
          <h3 className='mt-3'>Login into your account</h3>
          <div className='text-start mt-5'>
            <label style={{ fontWeight: "400", color: "#555555", fontSize: "20px" }}>Email Address</label>
            <div className='email-form mt-2'>
              <input className='inp-email' placeholder='alex@email.com'></input>
              <button className='btn btn-email' style={{ background: "#FD7401" }}>
                <i className="fa fa-envelope text-light"></i>
              </button>
            </div>
          </div>

          <div className='text-start mt-5'>
            <label style={{ fontWeight: "400", color: "#555555", fontSize: "20px" }}>Password</label>
            <div className='password-form mt-2'>
              <input className='inp-password' placeholder='Enter your password'></input>
              <button className='btn btn-password' style={{ background: "#FD7401" }}>
                <i className="fa fa-lock text-light"></i>
              </button>
            </div>
          </div>

          <div className='mt-3 text-end'>
            <a className='' style={{ fontWeight: "500", color: "darkblue" }}>Forgot Password?</a>
          </div>

          <div className='mt-5'>
            <button className='btn w-100' style={{ background: "#FD7401", height: "50px", color: "white", fontSize: "20px" }}>Login Now</button>
          </div>

          <div className='text-center div-or mt-4'>
            <hr className='w-25'></hr>
            <p> OR </p>
            <hr className='w-25'></hr>
          </div>

          <div className='mt-3'>
            <button className='btn btn-signup w-100' >Signup Now</button>
          </div>


        </div>
        <div className='col-lg-8' style={{background: "#F1F3F6"}}>
          <div className='pic-container container w-75 mx-auto'>
            <img src={picture} className='mx-auto'></img>

          </div>
        </div>

      </div>

    </div>
  );
}

export default App;
